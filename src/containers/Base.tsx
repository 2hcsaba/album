import React from 'react';

import '../css/base.css'

type alignVariations =
    | "flex-end"
    | "flex-start"
    | "center"
    | "baseline";

interface IBaseProps {
    alignContent: alignVariations,
    children: any
}

export default class Base extends React.Component<IBaseProps, any> {
    static defaultProps = {
        alignContent: "center"
    }

    public render() : JSX.Element {
        return (
            <main className={`flex flex-col md:flex-col w-full pt-5 pb-5 min-h-screen justify-${this.props.alignContent} sm:items-center sm:pb-0 sm:pt-0`}>
                <section className="content flex w-full h-full items-center md:justify-center">
                    {this.props.children}
                </section>
            </main>
        )
    }
}


