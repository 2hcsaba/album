import React from 'react'
import { Link } from 'react-router-dom'
import Title from '../components/Title'
import Base from './Base'

import PrivateNav from '../components/PrivateNav'
import AxiosInstance from '../service/api'
import PhotoList from '../components/PhotoList'

export interface PhotoElement {
    albumId: number,
    id: number, 
    title: string,
    url: string,
    thumbnailUrl: string
}

export interface PhotoState {
    photos: PhotoElement[],
    albumId: number | string,
    loading: boolean
}

export default class Album extends React.Component<any, PhotoState> {
    state = {
        photos: [],
        loading: true,
        albumId: '...'
    }

    async componentDidMount(): Promise<void> {
        try {
            const id =  parseInt(this.props.match.params.id)            

            this.setState({
                albumId: id
            })

            const response = await AxiosInstance(`/photos`)

            this.setState({
                photos: response.data.filter((photo: any) => parseInt(photo.albumId) === id),
                loading: false
            })
        } catch (error) {
            this.setState({
                loading: false
            })
        }
    }
    public render(): JSX.Element {
        return (
            <Base alignContent="flex-start">
                <section className="mt-5 sm:mt-10 w-full pl-6 pr-6 md:w-8/12">
                    <Link to="/albums" className="text-xs mb-8 inline-block">.. albums</Link>
                    <PrivateNav />
                    <Title name={`Photos of ${this.state.albumId}`} />
                    <div>
                        { !this.state.loading ? <PhotoList photos={this.state.photos} /> : <h1 className="font-bold text-normal text-center mt-10">loading...</h1>}
                    </div>
                </section>
            </Base>
        )
    }
}


