import React from 'react'
import { Link } from 'react-router-dom'
import Title from '../components/Title'
import Input from '../components/form/Input'
import Base from './Base'

interface ForgetState {
    emailOrPass: string
}

export default class Forgot extends React.Component<any, ForgetState> {
    state = {
        emailOrPass: ''
    }

    public render(): JSX.Element {
        return (
            <Base>
                <section className="w-full pl-6 pr-6 md:w-8/12">
                    <Title name="Forgot" />
                    <form onSubmit={(e) => { e.preventDefault(); alert('username: test \npassword: test') }}>
                        <Input value={this.state.emailOrPass} required={true} onChange={(e: any) => this.setState({ emailOrPass: e.target.value })} type="text" label="username or email" />
                        <div className="form-actions">
                            <Link className="text-sm" to="/login">or do you remember?</Link>
                            <button className="btn action mt-10">i don't :(</button>
                        </div>
                    </form>
                </section>
            </Base>
        )
    }
}