import React from 'react'
import Title from '../components/Title'
import Base from './Base'

import AxiosInstance from '../service/api'
import AlbumList from '../components/AlbumList'
import PrivateNav from '../components/PrivateNav'

export interface AlbumElement {
    userId: number,
    id: number,
    title: string
}

export interface AlbumState {
    albums: AlbumElement[],
    loading?: boolean
}

export default class Album extends React.Component<any, AlbumState> {
    state = {
        albums: [],
        loading: true
    }

    async componentDidMount(): Promise<void> {
        try {
            let result = await AxiosInstance.get('/albums')
            this.setState({
                albums: result.data,
                loading: false
            })
        } catch (error) {
            this.setState({
                loading: false
            })
        }
    }

    public render(): JSX.Element {
        return (
            <Base alignContent="flex-start">
                <section className="mt-5 sm:mt-10 w-full pl-6 pr-6 md:w-8/12">
                    <PrivateNav />
                    <Title name="Albums" />
                    <div className="overflow-x-hidden">
                        {!this.state.loading ? (<AlbumList albums={this.state.albums} />) : <h1 className="font-bold text-normal text-center mt-10">loading...</h1>}
                    </div>
                </section>
            </Base>
        )
    }
}


