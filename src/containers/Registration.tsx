import React from 'react'
import { Link } from 'react-router-dom'

import Input from '../components/form/Input'
import Title from '../components/Title'
//import VerifInput from '../components/form/VerifInput'

import '../components/css/button.css'
import '../components/css/form.css'
import auth from '../service/auth'
import Base from './Base'
import { IUser } from '../containers/Login'

export default class Registration extends React.Component<any, IUser> {
    state = {
        username: '',
        password: '',
        email: '',
        requested: false
    }

    private async handleRegister(e: any): Promise<void> {
        e.preventDefault()
        this.setState({ requested: true })
        try {
            const userData: IUser = {
                username: this.state.username,
                password: this.state.password,
                email: this.state.email
            }
            const user = await auth.register(userData)
            if (user) {
                console.log(auth.users)
                this.props.history.push('/login')
            }
        } catch (error) {
            console.log(error)
        }
        this.setState({ requested: false })
    }

    public render(): JSX.Element {
        return (
            <Base>
                <section className="w-full pl-6 pr-6 md:w-8/12">
                    <Title name="Registration" />
                    <form onSubmit={this.handleRegister.bind(this)}>
                        <Input value={this.state.username} required={true} type="text" label="username" onChange={(e: any) => this.setState({ username: e.target.value })} name="username"></Input>
                        <Input value={this.state.password} required={true} type="password" label="passwords" onChange={(e: any) => this.setState({ password: e.target.value })} name="password"></Input>
                        <Input value={this.state.email} type="email" onChange={(e: any) => this.setState({ email: e.target.value })} label="email"></Input>

                        <div className="form-actions">
                            <Link className="text-sm" to="/login">or perhaps login?</Link>
                            <button disabled={this.state.requested} className="btn action mt-10">
                                {this.state.requested ? 'loading...' : 'create'}
                            </button>
                        </div>
                    </form>
                </section>
            </Base>
        )
    }
}