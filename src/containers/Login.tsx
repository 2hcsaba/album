import React from 'react'

import Input from '../components/form/Input'
import Title from '../components/Title'

import { Link } from 'react-router-dom'
import Base from './Base'
import auth, { User } from '../service/auth'

export interface IUser extends User {
    requested?: boolean
}

export default class Login extends React.Component<any, IUser> {
    state = {
        username: '',
        password: '',
        requested: false
    }

    private async handleAuth(e: any): Promise<void> {
        e.preventDefault()
        this.setState({ requested: true })
        try {
            const userData: User = {
                username: this.state.username,
                password: this.state.password
            }
            console.log("try login w/ ", userData)
            const user = await auth.authenticate(userData)

            if (user) {
                this.props.history.push('/albums')
            }
        } catch (error) {
            console.log(error)
        }
        this.setState({ requested: false })
    }

    public render(): JSX.Element {
        return (
            <Base>
                <section className="w-full pl-6 pr-6 md:w-8/12">
                    <Title name="Login" />
                    <form onSubmit={this.handleAuth.bind(this)}>
                        <Input value={this.state.username} required={true} type="text" label="username" onChange={(e: any) => this.setState({ username: e.target.value })} />
                        <Input value={this.state.password} required={true} type="password" label="password" onChange={(e: any) => this.setState({ password: e.target.value })} >
                            <Link className="text-sm" to="/forgot">forgot :(</Link>
                        </Input>
                        <div className="form-actions">
                            <Link className="text-sm" to="/registration">click here to register</Link>
                            <button disabled={this.state.requested} className="btn action mt-10">
                                {this.state.requested ? 'loading...' : 'let me in'}
                            </button>
                        </div>
                    </form>
                </section>
            </Base>
        )
    }
}