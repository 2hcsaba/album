import React from 'react';

import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom'

import Login from './containers/Login'
import Registration from './containers/Registration'
import Forgot from './containers/Forgot'
import Album from './containers/Album'
import Photo from './containers/Photo'

import PrivateRoute from './_routes/PrivateRoute'

export default class App extends React.Component<any, any> {
  public render(): JSX.Element {
    return (
      <Router>
        <Switch>
          <Route path="/" exact component={() => <Redirect to="/login"></Redirect>} />

          <Route path="/login" component={Login} />
          <Route path="/forgot" component={Forgot} />
          <Route path="/registration" component={Registration} />

          <PrivateRoute path="/albums" component={Album} />
          <PrivateRoute path="/photos/:id" component={Photo} />
        </Switch>
      </Router>
    )
  }
}