import axios from 'axios'

const AxiosInstance = axios.create({
    baseURL: 'https://jsonplaceholder.typicode.com/',
    timeout: 10000,
    validateStatus: (status: number): boolean => {
        return status >= 200 && status < 300
    },
})

export default AxiosInstance