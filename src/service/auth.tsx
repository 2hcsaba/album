export interface User {
    username: string,
    password: string,
    email?: string
}

class Auth {
    authenticated: boolean
    users: User[]
    current?: User

    constructor() {
        this.authenticated = false
        this.users = []

        this.users.push({
            username: 'test',
            password: this.hash('password'),
            email: 'test@test.hu'
        })
    }

    private hash(password: string): string {
        return password
    }

    get isAuthenticated(): boolean {
        return true || this.authenticated
    }

    public register(user : User): Promise<any> {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                this.users.push({
                    username: user.username,
                    password: this.hash(user.password),
                    email: user.email
                })
                resolve(true)
            }, 1000)
        })
    }

    public logout(): void {
        this.authenticated = false
    }

    public authenticate(user: User): Promise<boolean> {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                const found = this.users.find(u => u.username === user.username && u.username === this.hash(user.password))
                if(found) {
                    this.authenticated = true
                    this.current = found
                    resolve(true)
                } else {
                    reject(false)
                }

            }, 1000)
        })
    }

    get username(): string {
        return this.current ? this.current.username : ''
    }

}

export default new Auth()