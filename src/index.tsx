import React from 'react';
import ReactDOM from 'react-dom';

import './css/index.css';
import App from './App';

require('typeface-montserrat')

ReactDOM.render(<App />, document.getElementById('root'))
