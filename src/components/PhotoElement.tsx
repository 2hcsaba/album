import React from 'react'
import { PhotoElement as IPhotoElement } from '../containers/Photo'

interface PhotoProps {
    className: string,
    photo: IPhotoElement,
    onClick(e: any): any,
    full: boolean,
}

interface PhotoState {
    loaded: boolean
}

export default class PhotoElement extends React.Component<PhotoProps, PhotoState> {
    static defaultProps = {
        onClick: (e: any) => null,
        full: false
    }

    state = {
        loaded: false
    }

    componentDidMount(): any {
        const url: string = this.props.full ? this.props.photo.url : this.props.photo.thumbnailUrl
        let image = new Image()
        image.src = url
        image.onload = (e) => {
            setTimeout(() => {
                this.setState({ loaded: true })
            }, 1000)
        }
    }



    public render(): JSX.Element {
        if (this.state.loaded) {
            return (
                <img style={ this.props.full ? {
                    width: "50vh",
                    maxWidth: "50vw"
                } : {}} className={this.props.className}
                    src={this.props.full ? this.props.photo.url : this.props.photo.thumbnailUrl}
                    data-id={this.props.photo.id}
                    alt={this.props.photo.title}
                    title={this.props.photo.title}
                    data-url={this.props.photo.url}
                    onClick={this.props.onClick.bind(this)}
                />
            )
        } else {
            return <p className="font-bold text-xs pb-10">image is loading..</p>
        }
    }
}