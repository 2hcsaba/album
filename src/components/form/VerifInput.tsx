import React from 'react'

import Input from './Input'

import './../css/verifinput.css'

interface VerifInputProp {
    label: string,
    type: string,   
    name: string,
    times: number,
    validator(str: string) : any,
    onChange(event: any): any
}

interface VerifInputState {
    versions: Array<string>,
    currentVersion: number,
    value: string,
    message: string
}

export default class VerifInput extends React.Component<VerifInputProp, VerifInputState> {
    static defaultProps = {
        name: 'default',
        onChange: () => null
    }
    
    constructor(props : VerifInputProp) {
        super(props)
        
        this.state = {
            currentVersion: 0,
            versions: [],
            value: '',
            message: `${this.props.times}x kell majd beírni..`
        }
    }
    
    private change(e : any) : void {
        this.setState({
            value: e.target.value
        })
    }
    
    private getRemaining() : number {
        return this.props.times - this.state.currentVersion
    }
    
    private isCurrentValid() : boolean {
        return this.props.validator(this.state.value)
    }
    
    private isMaxVersions() : boolean {
        return this.state.currentVersion === this.props.times 
    }
    
    private isEverythingMatches() : boolean {
        return this.state.versions.every((value : string, _, arr : Array<string>) => value === arr[0])
    }
    
    private keydown(e : any) : void {
        if(e.key === 'Enter') {
            this.handleNewVersion()
        } else {
            this.resetVersions()
        }
    }
    
    private resetVersions() : void {
        if(this.state.currentVersion >= this.props.times) {
            this.setState({
                currentVersion: 0,
                versions: [],
                value: ''
            })
        }
    }
    
    private handleNewVersion() : void {
        const cond = () => this.isEverythingMatches()  && this.isMaxVersions()
        
        if(!this.isCurrentValid()) {
            console.log('nem valid')
        }

        this.setState({
            currentVersion: this.state.currentVersion + 1,
            versions: [...this.state.versions, this.state.value],
            value: '',
        })
    }
    
    private addVersion() : void {
        this.handleNewVersion()
    }
    
    private getMessage() : string {        
        if(this.state.currentVersion >= 0 && this.state.currentVersion < this.props.times) {
            return `még ${this.getRemaining()}x`; 
        } else {
            return (this.isEverythingMatches() && this.isMaxVersions()) ? 'oké :)' : 'nem egyeznek :(';
        }
    }
    
    public render() : React.ReactNode {
        return (
            <Input value={this.state.value} label={this.props.label} type={this.props.type} name={this.props.name} onKeyDown={this.keydown.bind(this)} onChange={this.change.bind(this)}>
            <span className="message font-bold text-xs p-1 pl-2 pr-2 ">
            { this.getMessage() }
            </span>
            { !(this.isEverythingMatches() && this.isMaxVersions())  ? <button onClick={this.addVersion.bind(this)} className="message-helper absolute"></button> : ''  }
            </Input>
            )
        }
    }
    
    