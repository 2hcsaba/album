import React from 'react'

import '../css/input.css'

interface InputProp {
    label: string,
    type: string,   
    name: string,
    value: string,
    required?: boolean,
    onChange(event : any): any
    onKeyDown(event : any): any
}

interface InputState {
    type: string
}

export default class Input extends React.Component<InputProp, InputState> {
    static defaultProps = {
        name: 'default',
        value: '',
        required: false,
        onChange: (e: any) => null,
        onKeyDown: (e: any) => null
    }

    state = {
        type: this.props.type
    }
    
    private toggleType() : void {
        this.setState({
            type: this.state.type === 'password' ? 'text' : 'password'
        })
    }
    
    public render() : JSX.Element {
        return (
        <div className="album-input flex flex-col w-full mt-10 relative">
            <div className="flex justify-between items-baseline">
                <span className="font-bold text-sm sm:text-xl">
                    { this.props.label }
                    { this.props.type === 'password' ? 
                    <button className="ml-6 text-xs md:text-sm link" onClick={this.toggleType.bind(this)}>
                        { this.state.type === 'text' ? "dont't show" : 'show' }
                    </button> : '' }
                </span>
                { this.props.children }
            </div>
            <div className="input-outer">
                <input 
                required={ this.props.required }
                name={ this.props.name }
                type={ this.state.type } 
                onKeyDown={ this.props.onKeyDown.bind(this) }
                onChange={ this.props.onChange.bind(this) }
                className="font-bold pt-3 pb-3 sm:pt-4 sm:pb-4 w-full outline-none bg-transparent"/>
            </div>
        </div>
        )
    }
}

