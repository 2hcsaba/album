import React from 'react'

import './css/title.css'

interface Title {
    name: string
}

export default (props: Title) => {
    return (<h1 className="font-extrabold text-2xl sm:text-4xl pt-4 pb-4 sm:pb-8">{ props.name }</h1>)
}