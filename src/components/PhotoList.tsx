import React from 'react'
import { PhotoElement as IPhotoElement } from '../containers/Photo'
import PhotoElement from './PhotoElement'

interface PhotoListProps {
    photos: IPhotoElement[]
}

interface PhotoListState {
    opened: boolean,
    openedImage?: IPhotoElement
}

export default class AlbumList extends React.Component<PhotoListProps, PhotoListState> {
    state = {
        opened: false,
        openedImage: undefined
    }

    private openImage(e: any): void {
        const data = e.target.dataset

        this.setState({
            opened: true,
            openedImage: this.props.photos.find((photo: any) => parseInt(photo.id) === parseInt(data.id))
        })
    }

    public render(): any {
        return (
            <div>
                {
                    this.state.opened ?
                        <div>
                            <div className="fixed top-0 left-0 w-full h-full bg-album-beige opacity-75 z-10"></div>
                            <div onClick={() => this.setState({ opened: false })} className="fixed flex w-full h-full left-0 top-0 items-center justify-center flex-col z-20 cursor-pointer">
                                {
                                    this.state.openedImage ?
                                        //@ts-ignore
                                        <PhotoElement className="w-3/4 sm:w-1/2" photo={this.state.openedImage} full={true} />
                                        : ''
                                }
                            </div>
                        </div>
                        : ''
                }
                {
                    this.props.photos.length > 0 ?
                        <ul className="flex flex-wrap -mx-2 overflow-hidden mt-10 mb-12">
                            {
                                this.props.photos.map((photo: any) =>
                                    <li className="my-2 px-2 w-1/2 overflow-hidden md:w-1/3 lg:w-1/5" key={photo.id}>
                                        <PhotoElement className="w-full cursor-pointer" photo={photo} onClick={this.openImage.bind(this)} />
                                    </li>
                                )
                            }
                        </ul>
                        : <h1 className="font-bold text-normal text-center mt-10">the album is empty..</h1>
                }
            </div>
        )
    }
}