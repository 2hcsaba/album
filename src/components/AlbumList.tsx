import React from 'react'
import { Link } from 'react-router-dom'

import { AlbumState as AlbumProps, AlbumElement} from '../containers/Album'

export default class AlbumList extends React.Component<AlbumProps, any> {
    public render(): JSX.Element {
        if(this.props.albums.length > 0) {
            return (
                <ul className="flex flex-wrap -mx-16 overflow-hidden">
                {
                    this.props.albums.map((album: AlbumElement) =>
                        <li className="my-4 px-16 w-full overflow-hidden md:w-1/2" key={album.id}>
                            <Link to={"/photos/" + album.id}>{album.title}</Link>
                        </li>
                    )
                }
            </ul>
            )
        } else {
            return (<h1 className="font-bold text-normal text-center mt-10">there are no albums..</h1>)
        }
        
    }
}