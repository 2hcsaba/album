import React from 'react'
import { Link } from "react-router-dom"
import auth from "../service/auth"

export default class PrivateNav extends React.Component<any, any> {
    public render() : any {
        return (
            <div>
                <div className="flex flex-row justify-between items-baseline">
                    <h3 className="font-bold text-lg">hi, {auth.username}</h3>
                    <Link to="/login" onClick={() => auth.logout()}>logout</Link>
                </div>
            </div>
        )
    }
}