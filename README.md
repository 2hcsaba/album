![alt text](./public/192.png "Logo Title Text 1")

# ALBUM

#### 💡Demo is available at: [album-album-and-album.surge.sh](http://album-album-and-album.surge.sh)

## Story:
>Album is web application written in [React](https://reactjs.org/) using [Typescript](https://www.typescriptlang.org/). For less css typing this application is using
[TailwindCSS](https://tailwindcss.com/docs/installation/). HTTP requests handled by [Axios](https://github.com/axios/axios).

Fake data provider: [JSONPlaceholder](https://jsonplaceholder.typicode.com/guide.html).


## Instructions
#### Setup
Run:
```npm i``` or ```npm install```

#### For development
Run: `npm run start`
#### For production build
Run: `npm run build`
#### Test
Run: `npm run test`