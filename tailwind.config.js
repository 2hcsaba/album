const themeColors = {
  'album-beige': '#d6d5c9',
  'album-purple': '#554785',
  'album-darkgreen':'#2b331b',
  'album-yellowish': '#b5af64'
}

module.exports = {
  theme: {
    fontFamily: {
      'montserrat': "'Montserrat', sans-serif"
    },
    backgroundColor: theme => ({
      ...theme('colors'),
      ...themeColors
    }),
    extend: {
      colors: {
        ...themeColors
      }
    },
  },
  variants: {},
  plugins: [
    
  ],
}

